# Ruby Plate

```ruby
## File System

get_files()
get_directories()

get_fileinfo()
get_directoryinfo()

get_basepath()
get_fullpath()

get_filesize()
get_fileowner()
get_filegroup()

get_drives()
get_volumes()

get_freespace()
get_usedspace()
get_size/capacity()

create_file()
create_directory()
create_drive()
create_volume()

open_file()
close_file()
write_file()
read_file()

delete_file()
delete_directory()
delete_drive()
delete_volume()

## System

get_uptime()
get_systeminfo()

get_processor()
get_processorcount()
get_ramcapacity()
get_ramused()
get_ramfreespace()

get_gpuinfo()
get_monitorinfo()
get_resolution()
get_screencoords()

get_diskinfo()

## Processes & Threads

get_processbyid()
get_threadbyid()
get_fiberbyid()

get_processbyname()
get_threadbyname()
get_fiberbyname()

get_processlist()
get_processthreads()

create_process()
create_thread()
create_fiber()

terminate_process()
terminate_thread()
terminate_fiber()

## Networking & IPC

get_socketbyid()
get_pipebyid()

create_socket()
create_pipe()

write_socket()
read_socket()
write_pipe()
read_pipe()

close_socket()
close_pipe()

## Data Structures

create_set()
create_list()
create_queue()
create_map()
create_vector()
create_linkedlist()

## Concurrency & Synchronization

create_mutex()
create_semaphore()
create_timer()
create_event()
create_criticalsection()

## Date & Time

get_time()
get_date()

####################################################################################
####################################################################################
####################################################################################
####################################################################################


get_stringcontains
get_stringsplit
get_stringlength
get_stringbetween
get_stringlast
get_stringfirst
get_stringfromend
get_stringfromstart
get_stringcontains
get_stringsubstring
get_stringjoin
get_stringconcatenate

get_intfromstring
get_stringfromint
get_floatfromstring
get_stringfromfloat

get_desktoppath
get_userpath
get_appdatapath
get_windowspath
get_temppath
get_roamingpath





get_registrykey
get_clipboard
get_window
get_topmostwindow

get_batterylevel
get_volume
get_network

get_cipher
get_compression
get_encoding

get_html
get_webpage
get_json
get_xml
get_css

get_csv
get_ini
get_sql
get_http
get_ftp
get_rss

get_rand
get_salt/digest
get_regex
get_logger
get_levenshtein
get_ipaddress
get_line
get_lines

read_line
read_lines

get_base64
get_watir
get_mechanize
get_cookies
set_cookies

get_watirbutton
press_watirbutton

insert_watireditboxtext
get_watircss

execute_system(cmd)

set_test
get_test
run_test/s

run_rubocop



write_file
write_socket
write_pipe

terminate_thread
terminate_process

delete/eemove_file
delete/remove_directory


draw_rectangle
draw_box
draw_line
draw_circle
draw_fill
draw_triangle
draw_clear

get_icon
get_fsdb

get_stringadvanced

get_args


exists_process
exists_thread
exists_fiber
exists_string
exists_file
exists_socket
exists_window
exists_directory
exists_drive
exists_volume
exists_sql
exists_infile
exists_fsdb
exists_csv
exists_ini

create_window
create_drive
create_volume
create_string

get_drivefilesystem

get_os
get_osversion
get_machinebrand
get_hardwareinfo
get_username
get_


```
